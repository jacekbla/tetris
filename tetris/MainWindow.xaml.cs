﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.IO;
using System.Data.SqlClient;
using System.Data;

//dany rodzaj klocka zostaje obrocony nawet po tym jak juz lezy - tzn ze nastepny 
//jaki przyjdzie bedzie tak obrocony jak ten poprzedni byl kiedy zostal polozony - done

//TODO
//duchy - done
//hold - done
//wiecej nastepnych shape'ow?? - nie
//scalic board i colorBoard? - nie
//rotate w druga strone?
//obracanie zeby nie anulowalo poruszania w bok?
//spawnowanie klockow nad plansza - dodanie 2/3/4(?) rzedow, ktorych nie widac - meh ok
//uladnic
//statystyki

//opcje:
//gamespeed
//lines to remove
//skroty klawiszowe?
//kolorki??
//rozdzielczosc?

namespace tetris
{
    public partial class MainWindow : Window
    {
        const int ROW_COUNT = 20;
        const int COLUMN_COUNT = 10;
        const string optionsFile = "options.txt";
        const int START_GAMESPEED = 500;

        int LINES_TO_REMOVE = 10;
        int START_HEIGHT = 0;
        int START_WIDTH = 5;
        string defaultUserName = "John";
        int initialWait = 1;

        SqlConnection dbConnection;

        int[,] board = new int[ROW_COUNT + 2, COLUMN_COUNT + 2];
        int[,] colorBoard = new int[ROW_COUNT + 2, COLUMN_COUNT + 2];

        DispatcherTimer timer;
        DispatcherTimer timer_realTime;
        TimeSpan time;
        bool gameStarted = false;
        Random shapeRand;
        int currShape;
        int nextShape;
        int ghostHeight;
        int gamespeed;
        int currHeight;
        int currWidth;
        int linesRemoved = 0;
        int piecesCounter = 0;
        bool holdUsed = false;
        int heldId = -1;
        List<int[,]> tetrominos;
        List<int[,]> tetrominosNotRotated;
        List<Color> tetrominoColors;


        #region blocks definition

        int[,] tetromino_o = {
            { 0,0,0,0 },
            { 0,1,1,0 },
            { 0,1,1,0 },
            { 0,0,0,0 }
        };

        int[,] tetromino_i = {
            { 0,0,0,0 },
            { 1,1,1,1 },
            { 0,0,0,0 },
            { 0,0,0,0 }
        };

        int[,] tetromino_l = {
            { 0,0,1 },
            { 1,1,1 },
            { 0,0,0 }
        };

        int[,] tetromino_j = {
            { 1,0,0 },
            { 1,1,1 },
            { 0,0,0 }
        };

        int[,] tetromino_z = {
            { 1,1,0 },
            { 0,1,1 },
            { 0,0,0 }
        };

        int[,] tetromino_s = {
            { 0,1,1 },
            { 1,1,0 },
            { 0,0,0 }
        };

        int[,] tetromino_t = {
            { 0,1,0 },
            { 1,1,1 },
            { 0,0,0 }
        };

        int[,] tetromino_o_nr = {
            { 0,0,0,0 },
            { 0,1,1,0 },
            { 0,1,1,0 },
            { 0,0,0,0 }
        };

        int[,] tetromino_i_nr = {
            { 0,0,0,0 },
            { 1,1,1,1 },
            { 0,0,0,0 },
            { 0,0,0,0 }
        };

        int[,] tetromino_l_nr = {
            { 0,0,1 },
            { 1,1,1 },
            { 0,0,0 }
        };

        int[,] tetromino_j_nr = {
            { 1,0,0 },
            { 1,1,1 },
            { 0,0,0 }
        };

        int[,] tetromino_z_nr = {
            { 1,1,0 },
            { 0,1,1 },
            { 0,0,0 }
        };

        int[,] tetromino_s_nr = {
            { 0,1,1 },
            { 1,1,0 },
            { 0,0,0 }
        };

        int[,] tetromino_t_nr = {
            { 0,1,0 },
            { 1,1,1 },
            { 0,0,0 }
        };

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            connectToDB();
            updateDataGrid();

            toggleGrid(Menu_Grid, true);
            toggleGrid(Sprint_Grid, false);
            toggleGrid(Options_Grid, false);
            toggleGrid(History_Grid, false);
            toggleGrid(Stats_SprintGrid, false);

            Sprint_Button.Click += sprintButtonClick;
            Options_Button.Click += optionsButtonClick;
            History_Button.Click += historyButtonClick;
            Quit_Button.Click += quitButtonClick;

            Back_SprintButton.Click += backButtonClick;
            Back_OptionsButton.Click += backButtonClick;
            Back_HistoryButton.Click += backButtonClick;
            
            Start_SprintButton.Click += startButtonClick;

            Save_OptionsButton.Click += saveButtonClick;
            Save_SprintButton.Click += saveStatsButtonClick;

            gamespeed = START_GAMESPEED;
            time = new TimeSpan();
            timer_realTime = new DispatcherTimer();
            timer_realTime.Interval = new TimeSpan(0,0,0,0,100);
            timer_realTime.Tick += Timer_realTime_Tick;
            timer_realTime.Stop();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, gamespeed);
            timer.Tick += Timer_Tick;
            timer.Stop();
            KeyDown += Main_KeyDown;

            shapeRand = new Random();
            currShape = shapeRand.Next(0, 7);
            nextShape = shapeRand.Next(0, 7);

            tetrominos = new List<int[,]>();
            tetrominos.Add(tetromino_o);
            tetrominos.Add(tetromino_i);
            tetrominos.Add(tetromino_l);
            tetrominos.Add(tetromino_j);
            tetrominos.Add(tetromino_z);
            tetrominos.Add(tetromino_s);
            tetrominos.Add(tetromino_t);

            tetrominosNotRotated = new List<int[,]>();
            tetrominosNotRotated.Add(tetromino_o_nr);
            tetrominosNotRotated.Add(tetromino_i_nr);
            tetrominosNotRotated.Add(tetromino_l_nr);
            tetrominosNotRotated.Add(tetromino_j_nr);
            tetrominosNotRotated.Add(tetromino_z_nr);
            tetrominosNotRotated.Add(tetromino_s_nr);
            tetrominosNotRotated.Add(tetromino_t_nr);

            tetrominoColors = new List<Color>();
            //tetromino colors
            tetrominoColors.Add(Colors.Yellow);
            tetrominoColors.Add(Colors.Teal);
            tetrominoColors.Add(Colors.Orange);
            tetrominoColors.Add(Colors.Blue);
            tetrominoColors.Add(Colors.Red);
            tetrominoColors.Add(Colors.Green);
            tetrominoColors.Add(Colors.Purple);
            //ghost colors
            tetrominoColors.Add(Color.FromArgb(127, Colors.HotPink.R, Colors.HotPink.G, Colors.HotPink.B));
            tetrominoColors.Add(Color.FromArgb(127, Colors.HotPink.R, Colors.HotPink.G, Colors.HotPink.B));
            tetrominoColors.Add(Color.FromArgb(127, Colors.HotPink.R, Colors.HotPink.G, Colors.HotPink.B));
            tetrominoColors.Add(Color.FromArgb(127, Colors.HotPink.R, Colors.HotPink.G, Colors.HotPink.B));
            tetrominoColors.Add(Color.FromArgb(127, Colors.HotPink.R, Colors.HotPink.G, Colors.HotPink.B));
            tetrominoColors.Add(Color.FromArgb(127, Colors.HotPink.R, Colors.HotPink.G, Colors.HotPink.B));
            tetrominoColors.Add(Color.FromArgb(127, Colors.HotPink.R, Colors.HotPink.G, Colors.HotPink.B));
            
            currHeight = START_HEIGHT;
            currWidth = START_WIDTH;

            initBoard();
        }

        #region Button control

        private void sprintButtonClick(object sender, RoutedEventArgs e)
        {
            toggleGrid(Menu_Grid, false);
            toggleGrid(Sprint_Grid, true);
        }

        private void optionsButtonClick(object sender, RoutedEventArgs e)
        {
            toggleGrid(Menu_Grid, false);
            toggleGrid(Options_Grid, true);

            loadOptions();
            gamespeed_OptionsTB.Text = gamespeed.ToString();
            LinesToRemove_OptionsTB.Text = LINES_TO_REMOVE.ToString();
            InitialWait_OptionsTB.Text = initialWait.ToString();
            StartWidth_OptionsTB.Text = START_WIDTH.ToString();
            StartHeigth_OptionsTB.Text = START_HEIGHT.ToString();
            Name_OptionsTB.Text = defaultUserName;
        }

        private void historyButtonClick(object sender, RoutedEventArgs e)
        {
            toggleGrid(Menu_Grid, false);
            toggleGrid(History_Grid, true);
            updateDataGrid();
        }

        private void backButtonClick(object sender, RoutedEventArgs e)
        {
            toggleGrid(Menu_Grid, true);
            toggleGrid(Sprint_Grid, false);
            toggleGrid(Options_Grid, false);
            toggleGrid(History_Grid, false);
        }
        private void quitButtonClick(object sender, RoutedEventArgs e)
        {
            dbConnection.Close();
            System.Windows.Application.Current.Shutdown();
        }

        private void saveButtonClick(object sender, RoutedEventArgs e)
        {
            string gamespeed_input = gamespeed_OptionsTB.Text;
            string linesToRemove_input = LinesToRemove_OptionsTB.Text;
            string initialWait_input = InitialWait_OptionsTB.Text;
            string startWidth_input = StartWidth_OptionsTB.Text;
            string startheight_input = StartHeigth_OptionsTB.Text;
            string defaultName_input = Name_OptionsTB.Text;

            int gamespeed_i;
            int linesToRemove_i;
            int initialWait_i;
            int startWidth_i;
            int startHeight_i;

            if(int.TryParse(gamespeed_input, out gamespeed_i) 
                && int.TryParse(linesToRemove_input, out linesToRemove_i)
                && int.TryParse(initialWait_input, out initialWait_i)
                && int.TryParse(startWidth_input, out startWidth_i)
                && int.TryParse(startheight_input, out startHeight_i)
                && gamespeed_i > 0 && gamespeed_i < 2000
                && linesToRemove_i > 0 && linesToRemove_i < 101
                && initialWait_i > -1 && initialWait_i < 11
                && startWidth_i > 0 && startWidth_i < 11
                && startHeight_i > -1 && startHeight_i < 16)
            {
                gamespeed = gamespeed_i;
                LINES_TO_REMOVE = linesToRemove_i;
                initialWait = initialWait_i;
                START_WIDTH = startWidth_i;
                START_HEIGHT = startHeight_i;
                defaultUserName = defaultName_input;

                Error_OptionsLabel.Visibility = Visibility.Hidden;

                toggleGrid(Menu_Grid, true);
                toggleGrid(Sprint_Grid, false);
                toggleGrid(Options_Grid, false);
                toggleGrid(History_Grid, false);

                saveOptions();
            }
            else
            {
                Error_OptionsLabel.Visibility = Visibility.Visible;
            }
        }

        private void saveStatsButtonClick(object sender, RoutedEventArgs e)
        {
            saveToDB();
            MessageBox.Show("Saved!", "Done");
        }

        private void startButtonClick(object sender, RoutedEventArgs e)
        {
            loadOptions();
            timer.Interval = new TimeSpan(0, 0, 0, 0, gamespeed);
            timer.Start();
            timer_realTime.Start();
            gameStarted = true;
            currHeight = START_HEIGHT;
            currWidth = START_WIDTH;
            linesRemoved = 0;
            piecesCounter = 0;
            time = new TimeSpan();
            Start_SprintButton.IsEnabled = false;
            Back_SprintButton.IsEnabled = false;
            LinesRemoved_SprintLabel.Content = linesRemoved;
            addShape();
            calcGhostHeight();
            addGhostToBoard();
            renderBoard();
            drawNext();
            toggleGrid(Stats_SprintGrid, false);
            Name_Stats_SprintTB.Text = defaultUserName;
            Thread.Sleep(initialWait * 1000);
        }

        #endregion

        private void toggleGrid(Grid p_grid, bool p_enable)
        {
            p_grid.Opacity = p_enable ? 1 : 0;
            p_grid.IsEnabled = p_enable;
            p_grid.IsHitTestVisible = p_enable;
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            if (!timer.IsEnabled)
            {
                return;
            }
            if (Sprint_Grid.IsEnabled)
            {
                if (gameStarted)
                {
                    switch (e.Key.ToString())
                    {
                        case "Z":
                            if (currWidth > 0 && !checkCollisionRotationCCW())
                            {
                                removeShape();
                                deleteGhost();
                                rotateShape(tetrominos[currShape]);
                                calcGhostHeight();
                                addGhostToBoard();
                                addShape();
                                renderBoard();
                            }
                            break;
                        case "X":
                            if (currWidth > 0 && !checkCollisionRotationCW())
                            {
                                removeShape();
                                deleteGhost();
                                rotateShape(tetrominos[currShape]);
                                rotateShape(tetrominos[currShape]);
                                rotateShape(tetrominos[currShape]);
                                calcGhostHeight();
                                addGhostToBoard();
                                addShape();
                                renderBoard();
                            }
                            break;
                        case "Left":
                            if (!checkCollision(currWidth - 1, currHeight))
                            {
                                moveLeft();
                            }
                            break;
                        case "Right":
                            if (!checkCollision(currWidth + 1, currHeight))
                            {
                                moveRight();
                            }
                            break;
                        case "Down":
                            Timer_Tick(null, null);
                            break;
                        case "Space":
                            instantDown();
                            break;
                        case "C":
                            if(!holdUsed)
                            {
                                holdUsed = true;
                                removeShape();
                                deleteGhost();
                                if(heldId == -1)
                                {
                                    heldId = currShape;
                                    currShape = nextShape;
                                    nextShape = shapeRand.Next(0, 7);
                                }
                                else
                                {
                                    int tmpId = heldId;
                                    heldId = currShape;
                                    currShape = tmpId;
                                }
                                currHeight = START_HEIGHT;
                                currWidth = START_WIDTH;
                                addShape();
                                calcGhostHeight();
                                addGhostToBoard();
                                renderBoard();
                                drawNext();
                                drawHold();
                            }
                            break;
                        case "R":
                            restart();
                            break;
                    }
                }
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            showBoard();
            if (!checkCollision(currWidth, currHeight + 1))
            {
                moveShape();
            }
            else
            {//tetromino down
                holdUsed = false;
                piecesCounter++;
                //check for line removal
                int[,] tetromino = tetrominos[currShape];
                List<int> linesDoneId = new List<int>();
                for (int i = 0; i < tetromino.GetLength(0); i++)
                {
                    for (int j = 0; j < tetromino.GetLength(1); j++)
                    {
                        if (tetromino[i, j] == 1)
                        {
                            colorBoard[currHeight + i, currWidth + j] = currShape;
                        }
                    }
                    int h = currHeight + i;
                    if (h < ROW_COUNT + 1)
                    {
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[h, j] == 0)
                            {
                                j = board.GetLength(1);
                            }
                            if (j == board.GetLength(1) - 1 && board[h, j] == 1)
                            {
                                linesDoneId.Add(h);
                            }
                        }
                    }
                }
                
                linesDoneId.Reverse();
                int ctr = 0;
                foreach (int h in linesDoneId)
                {
                    int h_tmp = h + ctr;
                    for (int i = h_tmp; i > 1; i--)
                    {
                        for (int j = 1; j < board.GetLength(1) - 1; j++)
                        {
                            board[i, j] = board[i - 1, j];
                            colorBoard[i, j] = colorBoard[i - 1, j];
                        }
                    }
                    ctr++;
                }
                linesRemoved += ctr;
                LinesRemoved_SprintLabel.Content = linesRemoved;

                //unrotate for future uses
                Array.Copy(tetrominosNotRotated[currShape], tetrominos[currShape], 
                    tetrominos[currShape].GetLength(0) * tetrominos[currShape].GetLength(1));

                //check if game end
                bool maxHeightReached = false;
                for (int i = 1; i < board.GetLength(1) - 1 && !maxHeightReached; i++)
                {
                    maxHeightReached = board[START_HEIGHT + 2, i] == 1;
                }
                if (linesRemoved > LINES_TO_REMOVE - 1 || maxHeightReached)
                {//game ends
                    timer.Stop();
                    timer_realTime.Stop();
                    gameStarted = false;
                    currHeight = START_HEIGHT;
                    currWidth = START_WIDTH;
                    Start_SprintButton.IsEnabled = true;
                    Back_SprintButton.IsEnabled = true;

                    Pieces_Stats_SprintLabel.Content = "Pieces used: " + piecesCounter;
                    PPS_Stats_SprintLabel.Content = "Pieces/sec: " + (piecesCounter / time.TotalSeconds).ToString("0.00");
                    LinesMin_Stats_SprintLabel.Content = "Lines/min: " + (linesRemoved / time.TotalMinutes).ToString("0.00");
                    Lines_Stats_SprintLabel.Content = "Total lines: " + linesRemoved;
                    FinalTime_Stats_SprintLabel.Content = "Final time: " + time.ToString().Trim('0').TrimStart(':');
                    string gameResult = maxHeightReached ? "Fail" : "Success";
                    Win_Stats_SprintLabel.Content = "Game result: " + gameResult;
                    Save_SprintButton.IsEnabled = gameResult.Equals("Success");
                    toggleGrid(Stats_SprintGrid, true);

                    

                    NextTetromino_SprintGrid.Children.Clear();
                    Hold_SprintGrid.Children.Clear();
                    initBoard();
                    renderBoard();

                    //time = new TimeSpan();
                    //linesRemoved = 0;
                    //piecesCounter = 0;

                    heldId = -1;
                    holdUsed = false;
                }
                else
                {//add new shape
                    currShape = nextShape;
                    nextShape = shapeRand.Next(0, 7);
                    
                    currHeight = START_HEIGHT;
                    currWidth = START_WIDTH;
                    addShape();
                    calcGhostHeight();
                    addGhostToBoard();
                    renderBoard();
                    drawNext();
                }
            }
        }

        private void Timer_realTime_Tick(object sender, EventArgs e)
        {
            time = time.Add(new TimeSpan(0,0,0,0,100));
            Time_SprintLabel.Content = time.ToString().Trim('0').TrimStart(':');
        }

        private void loadOptions()
        {
            string[] lines = File.ReadAllLines(optionsFile);
            gamespeed = int.Parse(lines[0]);
            LINES_TO_REMOVE = int.Parse(lines[1]);
            initialWait = int.Parse(lines[2]);
            START_WIDTH = int.Parse(lines[3]);
            START_HEIGHT = int.Parse(lines[4]);
            defaultUserName = lines[5];
        }

        private void saveOptions()
        {
            string[] lines = {
                gamespeed.ToString(),
                LINES_TO_REMOVE.ToString(),
                initialWait.ToString(),
                START_WIDTH.ToString(),
                START_HEIGHT.ToString(),
                defaultUserName
            };

            System.IO.File.WriteAllLines(optionsFile, lines);
        }

        private void restart()
        {
            //stop
            timer.Stop();
            timer_realTime.Stop();
            time = new TimeSpan();
            gameStarted = false;
            currHeight = START_HEIGHT;
            currWidth = START_WIDTH;
            Start_SprintButton.IsEnabled = true;
            Back_SprintButton.IsEnabled = true;
            NextTetromino_SprintGrid.Children.Clear();
            Hold_SprintGrid.Children.Clear();
            initBoard();
            renderBoard();
            linesRemoved = 0;
            piecesCounter = 0;
            heldId = -1;
            holdUsed = false;

            //start
            timer.Start();
            timer_realTime.Start();
            gameStarted = true;
            currHeight = START_HEIGHT;
            currWidth = START_WIDTH;
            linesRemoved = 0;
            Start_SprintButton.IsEnabled = false;
            Back_SprintButton.IsEnabled = false;
            addShape();
            calcGhostHeight();
            addGhostToBoard();
            renderBoard();
            drawNext();
            
            Thread.Sleep(initialWait * 1000);
        }

        private void showBoard()
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    Console.Write(board[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        private void initBoard()
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                board[i, 0] = 1;
                board[i, board.GetLength(1) - 1] = 1;
            }
            for (int i = 0; i < board.GetLength(1); i++)
            {
                board[board.GetLength(0) - 1, i] = 1;
            }
            for (int i = 0; i < board.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < board.GetLength(1) - 1; j++)
                {
                    board[i, j] = 0;
                }
            }
        }

        private void drawNext()
        {
            NextTetromino_SprintGrid.Children.Clear();
            int[,] tetromino = tetrominosNotRotated[nextShape];
            for (int i = 0; i < tetromino.GetLength(0); i++)
            {
                for(int j = 0; j < tetromino.GetLength(1); j++)
                {
                    if(tetromino[i,j] == 1)
                    {
                        Rectangle rect = new Rectangle();
                        rect.Width = 20;
                        rect.Height = 20;
                        rect.Fill = new SolidColorBrush(tetrominoColors[nextShape]);
                        NextTetromino_SprintGrid.Children.Add(rect);
                        Grid.SetRow(rect, i);
                        Grid.SetColumn(rect, j);
                    }
                }
            }
        }

        private void drawHold()
        {
            Hold_SprintGrid.Children.Clear();
            int[,] tetromino = tetrominosNotRotated[heldId];
            for (int i = 0; i < tetromino.GetLength(0); i++)
            {
                for (int j = 0; j < tetromino.GetLength(1); j++)
                {
                    if (tetromino[i, j] == 1)
                    {
                        Rectangle rect = new Rectangle();
                        rect.Width = 20;
                        rect.Height = 20;
                        rect.Fill = new SolidColorBrush(tetrominoColors[heldId]);
                        Hold_SprintGrid.Children.Add(rect);
                        Grid.SetRow(rect, i);
                        Grid.SetColumn(rect, j);
                    }
                }
            }
        }

        private void renderBoard()
        {
            Tetris_Grid.Children.Clear();
            for (int i = 1; i < board.GetLength(0) - 1; i++)
            {
                for(int j = 1; j < board.GetLength(1) - 1; j++)
                {
                    if(board[i, j] > 0)
                    {
                        addSquare(i - 1, j - 1, colorBoard[i,j]);
                    }
                }
            }
        }

        private void addSquare(int row, int col, int shapeId)
        {
            Rectangle rect = new Rectangle();
            rect.Width = 20;
            rect.Height = 20;
            rect.Fill = new SolidColorBrush(tetrominoColors[shapeId]);
            Tetris_Grid.Children.Add(rect);
            Grid.SetRow(rect, row);
            Grid.SetColumn(rect, col);
        }

        private void removeShape()
        {
            int[,] tetromino = tetrominos[currShape];
            for (int i = 0; i < tetromino.GetLength(0); i++)
            {
                for (int j = 0; j < tetromino.GetLength(1); j++)
                {
                    if (tetromino[i, j] == 1)
                    {
                        board[currHeight + i, currWidth + j] = 0;
                    }
                }
            }
        }

        private void addShape()
        {
            int[,] tetromino = tetrominos[currShape];
            for(int i = 0; i < tetromino.GetLength(0); i++)
            {
                for(int j = 0; j < tetromino.GetLength(1); j++)
                {
                    if(tetromino[i,j] == 1)
                    {
                        board[currHeight + i, currWidth + j] = 1;
                        colorBoard[currHeight + i, currWidth + j] = currShape;
                    }
                }
            }
        }

        private void calcGhostHeight()
        {
            Dictionary<int, int> colsRows = new Dictionary<int, int>();
            int[,] tetromino = tetrominos[currShape];

            for(int j = 0; j < tetromino.GetLength(1); j++)
            {
                for(int i = tetromino.GetLength(0) - 1; i > -1; i--)
                {
                    if (tetromino[i, j] == 1)
                    {
                        colsRows.Add(currWidth + j, currHeight + i);
                        break;
                    }
                }
            }
            
            int minHeight = int.MaxValue;
            foreach(KeyValuePair<int, int> colRow in colsRows)
            {
                int heightCounter = 0;
                for(int i = colRow.Value + 1; board[i, colRow.Key] != 1; i++)
                {
                    heightCounter++;
                }
                if(heightCounter < minHeight)
                {
                    minHeight = heightCounter;
                }
            }
            
            ghostHeight = minHeight + currHeight;
        }

        private void addGhostToBoard()
        {
            int[,] tetromino = tetrominos[currShape];
            for (int i = 0; i < tetromino.GetLength(0); i++)
            {
                for (int j = 0; j < tetromino.GetLength(1); j++)
                {
                    if (tetromino[i, j] == 1)
                    {
                        board[i + ghostHeight, currWidth + j] = 2;
                        colorBoard[i + ghostHeight, currWidth + j] = currShape + 7;
                    }
                }
            }
        }

        private void deleteGhost()
        {
            int[,] tetromino = tetrominos[currShape];
            for (int i = 0; i < tetromino.GetLength(0); i++)
            {
                for (int j = 0; j < tetromino.GetLength(1); j++)
                {
                    if (tetromino[i, j] == 1)
                    {
                        board[i + ghostHeight, currWidth + j] = 0;
                    }
                }
            }
        }

        private void rotateShape(int[,] p_tetromino)
        {
            int N = p_tetromino.GetLength(0);
            for (int x = 0; x < N / 2; x++)
            {
                for (int y = x; y < N - x - 1; y++)
                {
                    int temp = p_tetromino[x, y];
                    p_tetromino[x, y] = p_tetromino[y, N - 1 - x];
                    p_tetromino[y, N - 1 - x] = p_tetromino[N - 1 - x, N - 1 - y];
                    p_tetromino[N - 1 - x, N - 1 - y] = p_tetromino[N - 1 - y, x];
                    p_tetromino[N - 1 - y, x] = temp;
                }
            }
        }

        private void moveShape()
        {
            removeShape();
            currHeight++;
            addShape();
            renderBoard();
        }

        private void moveRight()
        {
            removeShape();
            deleteGhost();
            currWidth++;
            calcGhostHeight();
            addGhostToBoard();
            addShape();
            renderBoard();
        }

        private void moveLeft()
        {
            removeShape();
            deleteGhost();
            currWidth--;
            calcGhostHeight();
            addGhostToBoard();
            addShape();
            renderBoard();
        }

        private bool checkCollision(int newWidth, int newHeight)
        {
            int[,] tetromino = tetrominos[currShape];
            int[,] boardCopy = (int[,])board.Clone();
            for (int i = 0; i < tetromino.GetLength(0); i++)
            {
                for (int j = 0; j < tetromino.GetLength(1); j++)
                {
                    if (tetromino[i, j] == 1)
                    {
                        boardCopy[currHeight + i, currWidth + j] = 0;
                    }
                }
            }
            for (int i = 0; i < tetromino.GetLength(0); i++)
            {
                for (int j = 0; j < tetromino.GetLength(1); j++)
                {
                    if (tetromino[i, j] == 1)
                    {
                        if (boardCopy[newHeight + i, newWidth + j] == 1)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool checkCollisionLeft(int newWidth)
        {
            int[,] tetromino = tetrominos[currShape];
            int colNum = -1;
            List<int> rowNums = new List<int>();

            bool found = false;
            for(int i = 0; !found; i++)
            {
                for(int j = 0; j < tetromino.GetLength(0); j++)
                {
                    if(tetromino[j, i] == 1)
                    {
                        found = true;
                        colNum = i;
                        rowNums.Add(j);
                    }
                }
            }
            foreach(int i in rowNums)
            {
                if (board[currHeight + i, newWidth + colNum] == 1)
                {
                    return true;
                }
            }
            return false;
        }

        private bool checkCollisionRight(int newWidth)
        {
            int[,] tetromino = tetrominos[currShape];
            int colNum = -1;
            List<int> rowNums = new List<int>();

            bool found = false;
            for (int i = tetromino.GetLength(1) - 1; !found; i--)
            {
                for (int j = 0; j < tetromino.GetLength(0); j++)
                {
                    if (tetromino[j, i] == 1)
                    {
                        found = true;
                        colNum = i;
                        rowNums.Add(j);
                    }
                }
            }
            foreach (int i in rowNums)
            {
                if (board[currHeight + i, newWidth + colNum] == 1)
                {
                    return true;
                }
            }
            return false;
        }

        private bool checkCollisionDown(int newHeight)
        {
            int[,] tetromino = tetrominos[currShape];
            int rowNum = -1;
            List<int> colNums = new List<int>();

            bool found = false;
            for (int i = tetromino.GetLength(0) - 1; !found; i--)
            {
                for (int j = 0; j < tetromino.GetLength(1); j++)
                {
                    if (tetromino[i, j] == 1)
                    {
                        found = true;
                        rowNum = i;
                        colNums.Add(j);
                    }
                }
            }
            foreach (int i in colNums)
            {
                if (board[newHeight + rowNum, currWidth + i] == 1)
                {
                    return true;
                }
            }
            return false;
        }

        private bool checkCollisionRotationCCW()
        {
            int[,] currTetrominoCopy = (int[,])tetrominos[currShape].Clone();
            int[,] boardCopy = (int[,])board.Clone();
            for(int i = 0; i < currTetrominoCopy.GetLength(0); i++)
            {
                for(int j = 0; j < currTetrominoCopy.GetLength(1); j++)
                {
                    if(currTetrominoCopy[i, j] == 1)
                    {
                        boardCopy[currHeight + i, currWidth + j] = 0;
                    }
                }
            }
            rotateShape(currTetrominoCopy);
            for (int i = 0; i < currTetrominoCopy.GetLength(0); i++)
            {
                for (int j = 0; j < currTetrominoCopy.GetLength(1); j++)
                {
                    if (currTetrominoCopy[i, j] == 1 && boardCopy[currHeight + i, currWidth + j] == 1)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool checkCollisionRotationCW()
        {
            int[,] currTetrominoCopy = (int[,])tetrominos[currShape].Clone();
            int[,] boardCopy = (int[,])board.Clone();
            for (int i = 0; i < currTetrominoCopy.GetLength(0); i++)
            {
                for (int j = 0; j < currTetrominoCopy.GetLength(1); j++)
                {
                    if (currTetrominoCopy[i, j] == 1)
                    {
                        boardCopy[currHeight + i, currWidth + j] = 0;
                    }
                }
            }
            rotateShape(currTetrominoCopy);
            rotateShape(currTetrominoCopy);
            rotateShape(currTetrominoCopy);
            for (int i = 0; i < currTetrominoCopy.GetLength(0); i++)
            {
                for (int j = 0; j < currTetrominoCopy.GetLength(1); j++)
                {
                    if (currTetrominoCopy[i, j] == 1 && boardCopy[currHeight + i, currWidth + j] == 1)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        
        private void instantDown()
        {
            removeShape();
            currHeight = ghostHeight;
            addShape();
            renderBoard();
            Timer_Tick(null, null);
        }

        public void connectToDB()
        {
            try
            {
                dbConnection = new SqlConnection(@"Server=(local);Database=TetrisMatches;Trusted_Connection=Yes;");
                dbConnection.Open();
            }
            catch
            {
                MessageBox.Show("db error");
            }
        }

        public void updateDataGrid()
        {
            //string Get_Data = "SELECT * FROM TetrisMatches.dbo.GAMES";

            string Get_Data = "SELECT " +
            "RANK() OVER(" +
            "ORDER BY[GameTime]" +
            ") as [Rank]," +
            "[GameTime], " +
            "[GameResult], " +
            "[LinesRemoved], " +
            "[LinesToRemove], " +
            "[PiecesUsed] as Pieces, " +
            "[PPS], " +
            "[LPM], " +
            "[UserName], " +
            "[Date] " +
            "FROM TetrisMatches.dbo.GAMES " +
            "WHERE [GameResult] = 'w'" +
            "ORDER BY [Rank]";
            

            SqlCommand cmd = dbConnection.CreateCommand();
            cmd.CommandText = Get_Data;

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("TetrisMatches.dbo.GAMES");
            sda.Fill(dt);
            
            Games_HistoryDataGrid.ItemsSource = dt.DefaultView;
        }

        public void saveToDB()
        {
            string gameResult = Win_Stats_SprintLabel.Content.ToString().Split(':')[1] == " Fail" ? "f" : "w";
            string Insert_Data = "INSERT INTO [TetrisMatches].dbo.GAMES" + 
                "(" +
                    "[GameTime], " +
                    "[Date], " +
                    "[UserName], " +
                    "[PPS], " +
                    "[LPM], " +
                    "[GameResult], " +
                    "[LinesRemoved], " +
                    "[LinesToRemove], " +
                    "[PiecesUsed]" +
                ")" +
                "VALUES" +
                "(" +
                    "'" + time.ToString().TrimEnd('0') + "'" + ", " +
                    "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "'" + ", " +
                    "'" + Name_Stats_SprintTB.Text + "'" + ", " +
                    (piecesCounter / time.TotalSeconds).ToString("0.00") + ", " +
                    (linesRemoved / time.TotalMinutes).ToString("0.00") + ", " +
                    "'" + gameResult + "'" + ", " +
                    linesRemoved + ", " +
                    LINES_TO_REMOVE + ", " +
                    piecesCounter +
                ")";

            Console.WriteLine(Insert_Data);

            SqlCommand cmd = dbConnection.CreateCommand();
            cmd.CommandText = Insert_Data;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            int affectedRows = cmd.ExecuteNonQuery();
        }
    }

}
